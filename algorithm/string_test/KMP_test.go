package string

import (
	"code/algorithm/string"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_KMP(t *testing.T) {
	assert.Equal(t, string.KmpSearch("aaa", "aa"), 1)
}
