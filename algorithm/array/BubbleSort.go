package array

//冒泡排序算法
//从0~n将最大值交换到最后面
//从0~n-1将最大值交换到最后面
func BubbleSort(arr []int) []int {
	if len(arr) < 2 {
		return arr
	}
	for j := len(arr) - 1; j > 0; j-- {
		for i := 0; i < j; i++ {
			if arr[i] > arr[i+1] {
				arr[i], arr[i+1] = arr[i+1], arr[i]
			}
		}
	}
	return arr
}
