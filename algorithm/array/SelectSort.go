package array

// 选择排序
// 从i开始选择最小的值 然后将值放入i位置
func SelectSort(arr []int) []int {
	if len(arr) < 2 {
		return arr
	}
	var minIndex = 0
	for i := 0; i < len(arr)-1; i++ {
		minIndex = i
		for j := i + 1; j < len(arr); j++ {
			if arr[j] < arr[minIndex] {
				minIndex = j
			}
		}
		arr[i], arr[minIndex] = arr[minIndex], arr[i]
	}
	return arr
}
