package string

func GetNext(str string) []int {
	nexts := make([]int, len(str)+1)
	nexts[0] = -1
	for i, j := 0, -1; i < len(str); {
		if j == -1 {
			i++
			j++
		} else if str[i] == str[j] {
			i++
			j++
			nexts[i] = j
		} else {
			j = nexts[j]
		}
	}
	return nexts
}

func KmpSearch(str, pattenStr string) int {
	nexts := GetNext(pattenStr)
	i, j := 0, -1
	for i < len(str) && j < len(pattenStr) {
		if j == -1 || str[i] == pattenStr[j] {
			i++
			j++
		} else {
			j = nexts[j]
		}
	}
	if j == len(pattenStr) {
		return i - j
	} else {
		return -1
	}
}
