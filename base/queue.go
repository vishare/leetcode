package base

type Queue struct {
	List []interface{}
}

func (queue *Queue) Push(data interface{}) {
	queue.List = append(queue.List, data)
}

func (queue *Queue) Peek() interface{} {
	if len(queue.List) == 0 {
		return nil
	}
	data := queue.List[0]
	queue.List = queue.List[1:]
	return data
}
