package base

type Stack struct {
	List []interface{}
}

func (stack *Stack) Push(data interface{}) {
	stack.List = append(stack.List, data)
}

func (stack *Stack) Pop() interface{} {
	if len(stack.List) == 0 {
		return nil
	}
	data := stack.List[len(stack.List)-1]
	stack.List = stack.List[0 : len(stack.List)-1]
	return data
}
