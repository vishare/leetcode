package main

import (
	"fmt"
)

func main(){
	fmt.Println(myAtoi("00000-42a1234"))
}

func myAtoi(s string) int {
	//s = strings.TrimLeft(s," ")
	strArr := []byte(s)
	num := 0
	plus := true
	space := true
	flag := false
	for _,val := range strArr{
		// 去除头部空格
		if val == ' ' && space{
			continue
		}
		space = false
		if (val < 48 || val > 57) && (val != 43 ) && val != 45{//不在可接受的字符范围 直接break
			break
		}
		//是否拿到符号
		if !flag{
			if val == '-'{
				plus = false
				flag = true
				continue
			}else if val == '+'{
				plus = true
				flag = true
				continue
			}
		}
		//非数字直接break
		if  val < 48 || val > 57 {
			break
		}
		// 判断数字的正负号
		if !plus{
			if( num * 10 + int(val - 48)) > (1<<31) {
				num = 1<<31
				break
			}
		}else{
			if( num * 10 + int(val - 48)) > (1<<31 -1) {
				num = 1<<31 -1
				break
			}
		}
		flag = true
		num *= 10
		num += int(val - 48)
	}
	if !plus {
		num = 0 - num
	}
	return num
}
