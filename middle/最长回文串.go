package main

import (
	"fmt"
	"strings"
)

func main() {
	fmt.Println(longestPalindrome("babadada"))
}

func longestPalindrome(s string) string {
	var pos, maxRight int = 0, 0
	var strByte []string = strings.Split(s, "")
	s = "#" + strings.Join(strByte, "#") + "#"
	var maxStr string = ""
	var lengthMap []int = make([]int, len(s))
	for i, _ := range s {
		if i < maxRight {
			lengthMap[i] = min(lengthMap[2*pos-i], maxRight-i)
		} else {
			lengthMap[i] = 1
		}
		for {
			if (i-lengthMap[i]) >= 0 && (i+lengthMap[i]) < len(s) && (s[i-lengthMap[i]] == s[i+lengthMap[i]]) {
				lengthMap[i]++
			} else {
				break
			}
		}
		if maxRight < (i + lengthMap[i] - 1) {
			maxRight = i + lengthMap[i] - 1
			pos = i
		}

		if len(maxStr) < 2*lengthMap[i]-1 {
			maxStr = s[i+1-lengthMap[i] : i+lengthMap[i]]
		}
		i++
	}
	return strings.Join(strings.Split(maxStr, "#"), "")
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}
func max(x, y int) int {
	if x > y {
		return x
	}
	return y
}
