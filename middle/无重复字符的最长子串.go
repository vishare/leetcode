package main

/**
给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度  来源：力扣（LeetCode）
*/
import (
	"fmt"
)

func main() {
	fmt.Println(lengthOfLongestSubstring("hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789hijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"))
}

func lengthOfLongestSubstring(s string) int {
	var lenMap = make(map[uint8]uint16)
	var strArr = []byte(s)
	s = ""
	var i uint16 = 0
	var fa uint16 = 0
	for index, val := range strArr {
		if lenMap[val] > i {
			i = lenMap[val]
		}
		if (uint16(index) - i + 1) > fa {
			fa = uint16(index) - i + 1
		}
		lenMap[val] = uint16(index) + 1
	}
	return int(fa)
}
