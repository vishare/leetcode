package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
	var tempNode *ListNode
	var tempNode1 *ListNode
	tempNode = l1
	tempNode1 = l2
	var needPlus int = 0
	var preNode *ListNode = nil
	var firstNode *ListNode
	var zeroNode = ListNode{0, nil}
	for {
		node := ListNode{(tempNode.Val + tempNode1.Val + needPlus) % 10, nil}
		if (tempNode.Val + tempNode1.Val + needPlus) >= 10 {
			needPlus = 1
		} else {
			needPlus = 0
		}
		if preNode == nil {
			preNode = &node
			firstNode = preNode
		} else {
			preNode.Next = &node
			preNode = &node
		}
		if tempNode.Next == nil && tempNode1.Next == nil { //都是最后的数据了 直接break
			break
		}
		if tempNode.Next == nil { //单独一个是nil  继续循环相加
			tempNode = &zeroNode
		} else {
			tempNode = tempNode.Next
		}
		if tempNode1.Next == nil { //单独一个是nil  继续循环相加
			tempNode1 = &zeroNode
		} else {
			tempNode1 = tempNode1.Next
		}
	}
	if needPlus > 0 {
		zeroNode.Val = 1
		preNode.Next = &zeroNode
	}
	return firstNode
}
