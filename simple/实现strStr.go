package main

import "fmt"

func main() {
	fmt.Println(strStr("","ba"))
}
func strStr(haystack string, needle string) int {
	var needleLen = len(needle)
	index := 0
	for {
		if index + needleLen > len(haystack){
			index = -1
			break
		}
		if haystack[index:index+needleLen] == needle{
			break
		}
		index ++
	}
	return index
}