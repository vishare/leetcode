package main

import (
	"bytes"
	"strconv"
)

func main() {
	
}

func countAndSay(n int) string {
	rs := "1"
	var buf bytes.Buffer
	for ;n>1;n--{
		rs +="$"
		for k,i:=0,1;i< len(rs);i++{
			if rs[i-1]!=rs[i]{
				buf.WriteString(strconv.Itoa(i-k))
				buf.WriteByte(rs[i-1])
				k=i
			}
		}
		rs = buf.String()
		buf.Reset()
	}
	return rs
}