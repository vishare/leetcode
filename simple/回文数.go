package main

import (
	"fmt"
)

func main(){
	fmt.Println(0,isPalindrome(0))
}

func isPalindrome(x int) bool {
	if x < 0 {
		return false
	}
	var tempArr [10]int
	i := 0
	for {
		if x >= 10{
			tempArr[i] = x%10
			x = x/10
		}else{
			tempArr[i] = x
			break
		}
		i++
	}
	var isPalindromeFlag bool = true
	j := i
	i = 0
	for i < j {
		if tempArr[i]!= tempArr[j]{
			isPalindromeFlag = false
		}
		i++
		j--
	}
	return isPalindromeFlag
}