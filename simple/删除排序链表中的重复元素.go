package main

import "fmt"
type ListNode struct {
   Val int
   Next *ListNode
}
func main() {
	fmt.Println(deleteDuplicates(&ListNode{1,&ListNode{1,&ListNode{2,nil}}}))
}
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func deleteDuplicates(head *ListNode) *ListNode {
	var tempNode  *ListNode = head
	for tempNode != nil && tempNode.Next != nil{
		if tempNode.Val == tempNode.Next.Val{
			tempNode.Next = tempNode.Next.Next
		}else{
			tempNode = tempNode.Next
		}
	}
	return head
}
