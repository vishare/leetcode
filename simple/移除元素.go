package main

import "fmt"

func main(){
	fmt.Println(removeElement([]int{3,2,2,3},3))
}


func removeElement(nums []int, val int) int {
	var lenNum int = len(nums)
	i:= 0
	for i < lenNum{
		if nums[i] == val{
			nums[i] = nums[lenNum-1]
			lenNum --
		}else{
			i++
		}
	}
	return lenNum
}