package main

import "fmt"

func main(){
	fmt.Println(isValid("{[]}}"))
}
func isValid(s string) bool {
	charMap := map[uint8]uint8{
		')':'(',
		']':'[',
		'}':'{',
	}
	tempStrArr := []byte(s)
	heapArr := []byte{}
	for _,val := range tempStrArr {
		if len(heapArr) <=0{
			heapArr = append(heapArr,val)
			continue
		}
		if heapArr[len(heapArr)-1] == charMap[val]{
			heapArr = heapArr[0:len(heapArr)-1]
			continue
		}else{
			heapArr = append(heapArr, val)
		}
	}
	if len(heapArr) == 0{
		return true
	}
	return false
}