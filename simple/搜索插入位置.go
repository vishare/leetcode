package main

import "fmt"

func main() {
	fmt.Println(searchInsert([]int{0,1,3,5},0))
}


func searchInsert(nums []int, target int) int {
	for key,val := range nums{
		if val == target || target < val{
			return key
		}
		if target > val{
			if key >= len(nums)-1 || target < nums[key+1]{
				return key+1
			}
		}
	}
	return len(nums)
}
