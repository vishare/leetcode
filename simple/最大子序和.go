package main

import "fmt"
//动态规划法求最大子序和
func main() {
	fmt.Println(maxSubArray([]int{-2}));
}
func maxSubArray(nums []int) int {
	if len(nums) <= 1{
		return nums[0]
	}
	var i int = 1
	var preNumMax = nums[0]
	var maxSum = nums[0]
	for {
		if preNumMax >0{
			preNumMax = preNumMax + nums[i]
		}else{
			preNumMax = nums[i]
		}
		if preNumMax > maxSum{
			maxSum = preNumMax
		}
		i++
		if i >= len(nums){
			break
		}
	}
	return maxSum
}