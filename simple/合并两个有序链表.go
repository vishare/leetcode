package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	//list := mergeTwoLists(&ListNode{1,&ListNode{2,&ListNode{5,nil}}},&ListNode{0,&ListNode{5,&ListNode{7,nil}}})
	list := mergeTwoLists(&ListNode{1, nil}, &ListNode{0, nil})
	for {
		fmt.Println(list.Val)
		if list.Next == nil {
			break
		}
		list = list.Next
	}
}

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
	if l1 == nil {
		return l2
	}
	if l2 == nil {
		return l1
	}
	if l1.Val > l2.Val {
		l1, l2 = l2, l1
	}
	var tempNode = l1
	var tempNode2 = l2
	for {
		if tempNode == nil && tempNode2 == nil {
			break
		}
		if tempNode.Val <= tempNode2.Val {
			if tempNode.Next != nil {
				if tempNode.Next.Val >= tempNode2.Val {
					tempNode.Next, tempNode2, tempNode2.Next, tempNode = tempNode2, tempNode2.Next, tempNode.Next, tempNode2
				} else {
					tempNode = tempNode.Next
				}
			} else {
				tempNode.Next, tempNode2, tempNode2.Next, tempNode = tempNode2, tempNode2.Next, tempNode.Next, tempNode2
			}
		}
		if tempNode == nil {
			tempNode = tempNode2
			break
		}
		if tempNode2 == nil {
			break
		}
	}
	return l1
}
