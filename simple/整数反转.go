package main
//给你一个 32 位的有符号整数 x ，返回 x 中每位上的数字反转后的结果。 如果反转后整数超过 32 位的有符号整数的范围 [−231,  231 − 1] ，就返回 0。 来源：力扣（LeetCode）
//
import (
	"fmt"
	"math"
)

func main(){
	fmt.Println(math.MaxInt32,reverse(math.MaxInt32))
}
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverse(x int) int {
	var flag bool = false
	if x < 0 {
		x = 0 - x
		flag = true
	}
	var tempInt int = 0;
	for{
		tempInt *= 10
		tempInt = tempInt +(x % 10)
		x = x /10
		if x == 0{
			break
		}
		if tempInt *10 > math.MaxInt32 || (flag &&( tempInt *10 > math.MaxInt32+1)){
			tempInt = 0
			break
		}
	}
	if flag{
		tempInt = 0 - tempInt
	}
	return tempInt
}
