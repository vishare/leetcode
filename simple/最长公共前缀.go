package main

import "fmt"

func main(){
	fmt.Println(longestCommonPrefix([]string{""}))
}
func longestCommonPrefix(strs []string) string {
	if len(strs) <= 0 {
		return ""
	}
	var index = 0
	var tempStr = strs[0]
	//strs = strs[1:]
	var breakFlag bool = false
	for !breakFlag {
		if len(strs)<=0{
			break
		}
		for _,val := range strs{
			if len(val)-1 < index || len(tempStr)-1 < index || val[index] != tempStr[index]{
				breakFlag = true
				break
			}
		}
		index ++
	}
	index --
	if index <= 0 {
		return ""
	}
	return tempStr[0:index]
}