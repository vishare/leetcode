package main

import "fmt"

func main(){
	fmt.Println(romanToInt("MCMXCIV"))
}

func romanToInt(s string) int {
	romanIntMap  := map[uint8]int{
		'I':1,
		'V':5,
		'X':10,
		'L':50,
		'C':100,
		'D':500,
		'M':1000,
	}
	strArr := []byte(s)
	var i uint8 = 0
	var num = 0
	for {
		if  i < uint8(len(strArr)-1) && romanIntMap[strArr[i]] < romanIntMap[strArr[i + 1]]{
			num += - romanIntMap[strArr[i]]
		}else{
			num +=  romanIntMap[strArr[i]]
		}
		if i >= uint8(len(strArr)-1){
			break
		}
		i++
	}
	return num
}