package main

import "fmt"

func main() {
	fmt.Println(climbStairs(2))
}
func climbStairs(n int) int {
	var num = 1
	var pre = 1
	var i = 2
	for {
		if i > n {
			break
		}
		num,pre = num + pre,num
		i++
	}
	return num
}
