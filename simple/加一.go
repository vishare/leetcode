package main

import "fmt"

func main() {
	fmt.Println(plusOne([]int{9,9}))
}
func plusOne(digits []int) []int {
	var i = len(digits) -1
	var need int  = 1
	for{
		if i >= 0 && need == 1{
			digits[i] += need
			need = (digits[i])/10
			digits[i] = (digits[i])%10
		}else{
			break
		}
		i--
	}
	if need == 1{
		digits = append([]int{1},digits...)
	}
	return digits
}