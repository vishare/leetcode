package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println(findMedianSortedArrays([]int{1, 2}, []int{3}))
}

func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
	count := len(nums1) + len(nums2)
	var leftIndex int = 0
	var rightIndex int = 0
	if count%2 == 0 {
		leftIndex = count/2 - 1
		rightIndex = leftIndex + 1
	} else {
		leftIndex = int(math.Ceil(float64(count)/2.0)) - 1
		rightIndex = leftIndex
	}
	var nums3 []int = make([]int, 0)

	if len(nums1) == 0 {
		nums3 = nums2
	}
	if len(nums2) == 0 {
		nums3 = nums1
	}
	if len(nums3) == 0 {
		var i, j int = 0, 0
		for {
			if (i > len(nums1)-1) && (j > len(nums2)-1) {
				break
			}
			if i > len(nums1)-1 {
				nums3 = append(nums3, nums2[j])
				j++
				continue
			}

			if j > len(nums2)-1 {
				nums3 = append(nums3, nums1[i])
				i++
				continue
			}
			if nums1[i] < nums2[j] {
				nums3 = append(nums3, nums1[i])
				i++
			} else {
				nums3 = append(nums3, nums2[j])
				j++
			}
			if len(nums3) > rightIndex {
				break
			}
		}
	}
	return float64(nums3[leftIndex]+nums3[rightIndex]) / 2.0

}

//func findMedianSortedArrays(nums1 []int, nums2 []int) float64 {
//	count := len(nums1) + len(nums2)
//	var leftIndex int  = 0
//	var rightIndex int  = 0
//	if count %2 == 0 {
//		leftIndex = count/2
//		rightIndex = leftIndex + 1
//	}else{
//		leftIndex= int(math.Ceil(float64(count)/2.0))
//		rightIndex = leftIndex
//	}
//	var index,i,j = 0,0,0
//	var leftValue,rightValue int
//	if len(nums1) == 0{
//		leftValue = nums2[0]
//	}else if len(nums2) == 0{
//		leftValue = nums1[0]
//	}else{
//		if nums1[i] > nums2[j]{
//			leftValue = nums2[j]
//		}else{
//			leftValue = nums1[i]
//		}
//	}
//	for{
//		index ++
//		if index > rightIndex{
//			break
//		}
//		if index == leftIndex{
//			if i > len(nums1) - 1{
//				leftValue = nums2[j]
//			}else if j> len(nums2) -1{
//				leftValue = nums1[i]
//			}else{
//				if nums1[i] < nums2[j]{
//					leftValue = nums1[i]
//				}else{
//					leftValue = nums2[j]
//				}
//			}
//		}
//		if index == rightIndex{
//			if i > len(nums1) - 1{
//				rightValue = nums2[j]
//			}else if j> len(nums2) -1{
//				rightValue = nums1[i]
//			}else{
//				if nums1[i] < nums2[j]{
//					rightValue = nums1[i]
//				}else{
//					rightValue = nums2[j]
//				}
//			}
//		}
//		if i > len(nums1) - 1{
//			j++
//			continue
//		}
//		if j> len(nums2) -1{
//			i ++
//			continue
//		}
//		if  nums1[i] > nums2[j]{
//			j ++
//		}else{
//			i ++
//		}
//	}
//	return float64(leftValue+rightValue)/2.0
//}
